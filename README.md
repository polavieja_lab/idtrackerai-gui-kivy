# idtrackerai-gui-kivy

⚠️ This package is deprecated! ⚠️

This repository has been deprecated and archived since it's no longer used by current versions of idtracker.ai.

---

This repository contains a kivy-based Graphical User Interface (GUI) for the multi-animal tracking software [idtracker.ai](https://idtracker.ai/) v3.

Note that although most of the functionalities of this GUI will work, some of them might fail as we are not supporting this GUI in the new [idtracker.ai](https://idtracker.ai/) v3.

The new supported GUI for the [idtracker.ai](https://idtracker.ai/) v3 is based on [Pyforms](https://pyforms.readthedocs.io/en/v4/). Learn more about how to the new version in the [documentation](https://idtrackerai.readthedocs.io/en/latest/index.html)


## Installation

If you want to use this GUI with idtracker.ai v3 you can install it following these steps.

##### 1. Clone this repository

    git clone https://gitlab.com/polavieja_lab/idtrackerai-gui-kivy

##### 2. Create a conda environment with Python 3.6.

    conda create -n idtrackerai_gui_kivy python=3.6

##### 4. Activate the conda environment.

    conda activate idtrackerai_gui_kivy

##### 5. Go to the folder of the repository

    cd idtrackerai-gui-kivy

##### 6. Install the GUI. Note that this command will install idtracker.ai without GUI support and without GPU support.

    pip install -e .

##### 7. If you want to run idtracker.ai with GPU support we recoomend installing Tensorflow 1.13 from the Conda package manager.

    conda install tensorflow-gpu=1.13

##### 8. Install matplotlib from "garden"

    garden install matplotlib

## Open the GUI

To open this GUI run the command

    idtrackeraiGUI
